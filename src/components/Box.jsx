import React from "react";

export default function Box({ height = 25, width = 25, children }) {
  return (
    <div
      style={{
        height: `${height}px`,
        width: `${width}px`,
        border: "1px solid black",
      }}
    >
      {children}
    </div>
  );
}
