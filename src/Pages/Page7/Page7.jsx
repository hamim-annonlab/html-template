import React from "react";
import { Box } from "../../components";
import "../../styles/page7.css";
import Header from "./Header";

export default function Page7() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <Header />
      <div className='section1'>
        <div className='section1-row1'>
          <div className='exercice'>
            <small style={{ marginRight: "30px" }}>Exercice clos le</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='siren' style={{ marginLeft: "50px" }}>
            <small style={{ marginRight: "30px" }}>SIREN</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
        </div>
        <div className='section1-row2'>
          <div className='exercice'>
            <small style={{ marginRight: "70px" }}>
              Dénomination de l'entreprise
            </small>
            <div className='boxes'>
              <div
                style={{
                  height: "20px",
                  width: "300px",
                  borderBottom: "2px solid black",
                }}
              ></div>
            </div>
          </div>
        </div>
        <div className='section1-row3'>
          <div className='exercice'>
            <small style={{ marginRight: "50px" }}>Adresse (voie)</small>
            <div className='boxes'>
              <div
                style={{
                  height: "20px",
                  width: "400px",
                  borderBottom: "2px solid black",
                }}
              ></div>
            </div>
          </div>
        </div>
        <div className='section1-row4'>
          <div className='exercice'>
            <small style={{ marginRight: "30px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='siren' style={{ marginLeft: "50px" }}>
            <small style={{ marginRight: "30px" }}>Ville</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section-small'>
        <div style={{ borderRight: "2px solid black" }}>
          <small style={{ marginRight: "50px" }}>
            I – NOMBRE TOTAL DE FILIALES DÉTENUES PAR L'ENTREPRISE :
          </small>
        </div>
        <div style={{ borderRight: "2px solid black" }}>
          <small style={{ marginRight: "10px", marginLeft: "10px" }}>905</small>
        </div>
        <div style={{ width: "200px", borderRight: "2px solid black" }}>
          <small style={{ marginRight: "10px", marginLeft: "10px" }}>
            &nbsp;
          </small>
        </div>
        <div style={{ width: "554px", backgroundColor: "gray" }}>
          <small>&nbsp;</small>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='section2'>
        <div className='row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
        </div>
        <div className='row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <small style={{ marginBottom: "20px", marginTop: "5px" }}>
          (1) Lorsque le nombre de filiales excède le nombre de lignes du
          formulaire, veuillez utiliser un ou plusieurs tableaux
          supplémentaires. Dans ce cas, il convient de numéroter chaque tableau
          en haut et à gauche de la case prévue à cet effet et de porter le
          nombre total de tableaux souscrits en bas à droite de cette même case.
        </small>
        <small>
          * Des explications concernant cette rubrique figurent dans la notice
          n° 2033-NOT-SD
        </small>
      </div>
    </div>
  );
}
