import React from "react";
import Box from "../../components/Box";
import "../../styles/Header7.css";

export default function Header() {
  return (
    <div className='container' style={{ marginTop: "50px" }}>
      <div className='header7-row1'>
        <div>
          <p
            style={{
              height: "20px",
              width: "20px",
              border: "2px solid black",
              borderRadius: "50%",
              textAlign: "center",
            }}
          >
            7
          </p>
        </div>
        <div>
          <span
            style={{
              marginLeft: "40px",
              padding: "5px 40px",
              border: "2px solid black",
            }}
          >
            FILIALES ET PARTICIPATIONS
          </span>
        </div>
        <div style={{ marginLeft: "300px", textAlign: "center" }}>
          <p> DGFiP N° 2033-G-SD 2022</p>
          <small>
            Formulaire obligatoire (article 38 de l'annexe III au CGI)
          </small>
        </div>
      </div>
      <div className='header7-row2'>
        <div className='header7-section1'>
          <small>N° de dépôt</small>
          <Box height={25} width={100} />
        </div>
        <div
          style={{
            textAlign: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <small>
            (Liste des personnes ou groupements de personnes de droit ou de fait
            dont
          </small>
          <small>
            la société détient directement au moins 10 % du capital)
          </small>
        </div>
        <div className='header7-section3'>
          <Box height={25} width={100} />
          <small style={{ marginLeft: "5px" }}>(1)</small>
        </div>
        <div className='header7-section4'>
          <small>Néant</small>
          <Box height={10} width={10} /> *
        </div>
      </div>
    </div>
  );
}
