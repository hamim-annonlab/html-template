import React from "react";

export default function Row1({
  text,
  styles = { width: "100%", border: "1px solid black" },
}) {
  return (
    <div style={styles}>
      <small style={{ fontWeight: "bold", padding: "5px 2px" }}>{text}</small>
    </div>
  );
}
