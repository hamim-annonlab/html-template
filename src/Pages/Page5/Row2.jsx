import React from "react";

export default function Row2({ text, number }) {
  return (
    <div
      style={{
        width: "1200px",
        border: "1px solid black",
        display: "flex",
        flexDirection: "row",
      }}
    >
      <div style={{ width: "1000px", borderRight: "1px solid black" }}>
        <small>{text}</small>
      </div>
      <div
        style={{
          width: "50px",
          borderRight: "1px solid black",
          textAlign: "center",
        }}
      >
        <small>{number}</small>
      </div>
      <div style={{ width: "150px" }}>
        <small>&nbsp;</small>
      </div>
    </div>
  );
}
