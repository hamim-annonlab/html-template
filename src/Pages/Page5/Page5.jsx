import React from "react";
import Box from "../../components/Box";
import "../../styles/Page5.css";
import Row1 from "./Row1";
import Row2 from "./Row2";
import Total from "./Total";

export default function Page5() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <div className='p5header'>
        <div>
          <p
            style={{
              height: "20px",
              width: "20px",
              border: "2px solid black",
              borderRadius: "50%",
              textAlign: "center",
            }}
          >
            5
          </p>
        </div>
        <span
          style={{
            marginLeft: "40px",
            padding: "5px 40px",
            border: "2px solid black",
          }}
        >
          DÉTERMINATION DES EFFECTIFS ET DE LA VALEUR AJOUTÉE
        </span>
        <div style={{ marginLeft: "250px", textAlign: "center" }}>
          <small>DGFiP N° 2033-E-SD 2022</small>
        </div>
      </div>
      <div className='p5section1'>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div style={{ width: "300px", marginRight: "100px" }}>
            <small>
              Formulaire obligatoire (article 302 septies A bis du Code général
              des impôts)
            </small>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <small style={{ marginRight: "31px" }}>
              Désignation de l'entreprise
            </small>
            <div
              style={{
                height: "20px",
                width: "400px",
                borderBottom: "2px solid black",
              }}
            ></div>
          </div>
          <div
            style={{
              width: "100px",
              height: "20px",
              border: "1px solid black",
              marginLeft: "117px",
              marginTop: "-10px",
            }}
          >
            <div style={{ padding: "2px", display: "flex" }}>
              <small style={{ marginRight: "5px" }}>Neant</small>
              <Box height={10} width={10} /> *
            </div>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            padding: "15px 0 10px",
          }}
        >
          <small>
            Exercice ouvert le :
            .........................................................
          </small>
          <small style={{ marginLeft: "50px" }}>
            et clos le :
            .........................................................
          </small>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginLeft: "150px",
            }}
          >
            <small>Données en nombre de mois</small>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginLeft: "30px",
              }}
            >
              <Box height={25} width={25} />
              <Box height={25} width={25} />
            </div>
          </div>
        </div>
      </div>
      <div className='declaration'>
        <Row1 text='DÉCLARATION DES EFFECTIFS' />
        <Row2 text='Effectif moyen du personnel * :' number={376} />
        <div
          style={{
            width: "1200Px",
            border: "1px solid black",
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div style={{ width: "148px", borderRight: "1px solid black" }}>
            <small>&nbsp;</small>
          </div>
          <div
            style={{
              width: "849px",
              borderRight: "1px solid black",
            }}
          >
            <small>Dont apprentis</small>
          </div>
          <div
            style={{
              width: "50px",
              borderRight: "1px solid black",
              textAlign: "center",
            }}
          >
            <small>657</small>
          </div>
          <div style={{ width: "150px" }}>
            <small>&nbsp;</small>
          </div>
        </div>
        <div
          style={{
            width: "1200px",
            border: "1px solid black",
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div style={{ width: "148px", borderRight: "1px solid black" }}>
            <small>&nbsp;</small>
          </div>
          <div
            style={{
              width: "849px",
              borderRight: "1px solid black",
            }}
          >
            <small>Dont handicapés </small>
          </div>
          <div
            style={{
              width: "50px",
              borderRight: "1px solid black",
              textAlign: "center",
            }}
          >
            <small>651</small>
          </div>
          <div style={{ width: "150px" }}>
            <small>&nbsp;</small>
          </div>
        </div>
        <Row2 text="Effectifs affectés à l'activité artisanale" number={861} />
      </div>
      <div className='calcul'>
        <Row1 text='CALCUL DE LA VALEUR AJOUTÉE' />
      </div>
      <div className='chiffre'>
        <Row1 text="I – Chiffre d'affaires de référence CVAE" />
        <Row2
          text='Ventes de produits fabriqués, prestations de services et
              marchandises'
          number={108}
        />
        <Row2
          text='Redevances pour concessions, brevets, licences et assimilées'
          number={118}
        />
        <Row2
          text="Plus-values de cession d'immobilisations corporelles ou
              incorporelles si rattachées à une activité normale et courante"
          number={119}
        />
        <Row2
          text='Refacturations de frais inscrites au compte de transfert de
              charges'
          number={105}
        />
        <Total text='TOTAL 1' number={106} />
      </div>
      <div className='autres'>
        <Row1 text='II – Autres produits à retenir pour le calcul de la valeur ajoutée' />
        <Row2
          text='Autres produits de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)'
          number={115}
        />
        <Row2
          text='Production immobilisée à hauteur des seules charges déductibles ayant concouru à sa formation'
          number={143}
        />
        <Row2 text="Subventions d'exploitation reçues" number={113} />
        <Row2 text='Variation positive des stocks' number={111} />
        <Row2
          text='Transferts de charges déductibles de la valeur ajoutée'
          number={116}
        />
        <Row2
          text="Rentrées sur créances amorties lorsqu'elles se rapportent au résultat d'exploitation"
          number={153}
        />
        <Total text='TOTAL 2' number={144} />
      </div>
      <div className='charges'>
        <Row1 text='III – Charges à retenir pour le calcul de la valeur ajoutée (1)' />
        <Row2 text='Achats' number={121} />
        <Row2 text='Variation négative des stocks' number={145} />
        <Row2
          text="Services extérieurs, à l'exception des loyers et des redevances"
          number={125}
        />
        <Row2
          text="Loyers et redevances, à l'exception de ceux afférents à des immobilisations corporelles mises à disposition dans le cadre d'une convention de location-gérance ou de crédit-bail ou encore d'une convention de location de plus de 6 mois."
          number={310}
        />
        <Row2 text='Taxes déductibles de la valeur ajoutée' number={133} />
        <Row2
          text='Autres charges de gestion courante (hors quotes-parts de résultat sur opérations faites en commun)'
          number={148}
        />
        <Row2
          text='Charges déductibles de la valeur ajoutée afférente à la production immobilisée déclarée'
          number={128}
        />
        <Row2
          text="Fraction déductible de la valeur ajoutée des dotations aux amortissements afférentes à des immobilisations corporelles mises à disposition dans le cadre d'une convention de location-gérance ou de crédit-bail ou encore d'une convention de location de plus de 6 mois"
          number={135}
        />
        <Row2
          text="Moins-values de cession d'immobilisations corporelles ou incorporelles si rattachées à une activité normale et courante"
          number={150}
        />
        <Total text='TOTAL 3' number={152} />
      </div>
      <Row1 text='IV – Valeur ajoutée produite' />
      <div
        style={{
          width: "1200px",
          border: "1px solid black",
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            width: "1000px",
            borderRight: "1px solid black",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <small>Calcul de la valeur ajoutée</small>
          <small className='total'>(Total 1 + Total 2 - Total 3)</small>
        </div>
        <div
          style={{
            width: "50px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>137</small>
        </div>
        <div style={{ width: "150px" }}>
          <small>&nbsp;</small>
        </div>
      </div>
      <Row1 text='V – Cotisation sur la valeur ajoutée des entreprises' />
      <Row2
        text='Valeur ajoutée assujettie à la CVAE (à reporter sur le formulaire n° 1330-CVAE-SD pour les multi-établissements et sur les formulaires nos 1329-AC et 1329-DEF). Si la VA calculée est négative, il convient de reporter un montant égal à 0 au cadre C des formulaires nos 1329-AC et 1329-DEF'
        number={117}
      />
      <Row1
        text='Cadre réservé au mono-établissement au sens de la CVAE'
        styles={{
          width: "100%",
          border: "1px solid black",
          textAlign: "center",
        }}
      />
      <Row1 text='Si vous êtes assujettis à la CVAE et mono-établissement au sens de la CVAE (cf. notice du formulaire n° 1330-CVAE-SD), veuillez compléter le cadre ci-dessous et la case 117, vous serez alors dispensés du dépôt du formulaire n° 1330-CVAE-SD.' />
      <Row1 text='Les entreprises effectuant uniquement des opérations à caractère agricole n’entrant pas dans le champ de la CVAE ne doivent pas compléter ce cadre.' />
      <div
        style={{
          width: "1200px",
          border: "1px solid black",
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div style={{ width: "554px", borderRight: "1px solid black" }}>
          <small>
            Mono-établissement au sens de la CVAE, cocher la case ci-contre
          </small>
        </div>
        <div
          style={{
            width: "50px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>020</small>
        </div>
        <div style={{ width: "50px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "551px",
            borderRight: "1px solid black",
            backgroundColor: "grey",
          }}
        ></div>
      </div>
      <div
        style={{
          width: "1200px",
          border: "1px solid black",
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            width: "1000px",
            borderRight: "1px solid black",
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div style={{ width: "550px", borderRight: "1px solid black" }}>
            <small>
              Chiffre d'affaires de référence CVAE (report de la ligne 106, le
              cas échéant ajusté à 12 mois)
            </small>
          </div>
          <div
            style={{
              width: "50px",
              borderRight: "1px solid black",
              textAlign: "center",
            }}
          >
            <small>020</small>
          </div>
          <div style={{ width: "160px", borderRight: "1px solid black" }}>
            <small>&nbsp;</small>
          </div>
          <div style={{ width: "235px" }}>
            <small>Effectifs au sens de la CVAE *</small>
          </div>
        </div>
        <div
          style={{
            width: "50px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>023</small>
        </div>
        <div style={{ width: "150px" }}>
          <small>&nbsp;</small>
        </div>
      </div>
      <Row2
        text="Chiffre d'affaires du groupe économique (entreprises répondant aux conditions de détention fixées à l'article 223 A du CGI)"
        number='026'
      />
      <div
        style={{
          width: "1200px",
          border: "1px solid black",
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div style={{ width: "250px", borderRight: "1px solid black" }}>
          <small>Période de référence</small>
        </div>
        <div style={{ width: "50px", borderRight: "1px solid black" }}>
          <small>024</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "50px", borderRight: "1px solid black" }}>
          <small>016</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "348px", backgroundColor: "grey" }}></div>
      </div>
      <div
        style={{
          width: "1200px",
          border: "1px solid black",
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div style={{ width: "301px", borderRight: "1px solid black" }}>
          <small>Date de cessation</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div
          style={{
            width: "25px",
            borderRight: "1px solid black",
            textAlign: "center",
          }}
        >
          <small>/</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>
        <div style={{ width: "25px", borderRight: "1px solid black" }}>
          <small>&nbsp;</small>
        </div>

        <div style={{ width: "647px", backgroundColor: "grey" }}></div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <small style={{ marginTop: "5px" }}>
          (1) Attention, il ne doit pas être tenu compte dans les lignes 121 à
          148 des charges déductibles de la valeur ajoutée, afférentes à la
          production immobilisée déclarée ligne 143, portées en ligne 128.
        </small>
        <small style={{ marginBottom: "20px", marginTop: "5px" }}>
          * Des explications concernant ces rubriques figurent dans la notice n°
          2033-NOT-SD, au § « Déclaration des effectifs » et dans la notice n°
          1330-CVAE-SD, au § « Répartition des salariés »
        </small>
      </div>
    </div>
  );
}
