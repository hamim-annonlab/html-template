import React from "react";
import Box from "../../components/Box";
import "../../styles/Page4.css";

export default function Page4() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <div className='p4header'>
        <div>
          <p
            style={{
              height: "20px",
              width: "20px",
              border: "2px solid black",
              borderRadius: "50%",
              textAlign: "center",
            }}
          >
            4
          </p>
        </div>
        <span
          style={{
            marginLeft: "40px",
            padding: "5px 40px",
            border: "2px solid black",
          }}
        >
          RELEVÉ DES PROVISIONS – AMORTISSEMENTS DÉROGATOIRES – DÉFICITS
        </span>
        <div style={{ marginLeft: "250px", textAlign: "center" }}>
          <small>DGFiP N° 2033-D-SD 2022</small>
        </div>
      </div>
      <div className='p4r1'>
        <div className='formulaire'>
          <small>
            Formulaire obligatoire (article 302 septies A bis du Code général
            des impôts)
          </small>
        </div>
        <div className='designation'>
          <small>Désignation de l'entreprise</small>
          <div></div>
        </div>
        <div className='neant'>
          <small style={{ marginRight: "10px" }}>Néant</small>
          <Box height={10} width={10} /> *
        </div>
      </div>
      <div className='p4r2'>
        <div className='borderR'>
          <small>I</small>
        </div>
        <div>
          <small>RELEVÉ DES PROVISIONS – AMORTISSEMENTS DÉROGATOIRES</small>
        </div>
      </div>
      <div className='p4table1'>
        <div className='p4table1r2 borderB'>
          <div
            style={{
              width: "1200px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                style={{ width: "398px", fontWeight: "bold" }}
                className='borderR'
              >
                <small>A NATURE DES PROVISIONS</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                <small>Montant au début de l'exercice</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                <small>Augmentations : dotations de l'exercice</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                <small>Diminutions : reprises de l'exercice</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "151px" }}>
                <small>Montant à la fin de l'exercice</small>
              </div>
            </div>
          </div>
        </div>
        <div className='p4table1r2 borderB'>
          <div
            style={{
              width: "100px",
              display: "flex",
              alignItems: "center",
            }}
            className='borderR '
          >
            <small>Provisions réglementées</small>
          </div>
          <div
            style={{
              width: "1100px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR borderB'>
                <small>Amortissements dérogatoires</small>
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>600</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>602</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>604</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>606</small>
              </div>
              <div style={{ width: "151px" }} className=' borderB'>
                &nbsp;
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR borderB'>
                <small>Dont majorations exceptionnelles de 30 %</small>
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>601</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>603</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>605</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>607</small>
              </div>
              <div style={{ width: "151px" }} className=' borderB'>
                &nbsp;
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR'>
                <small>Autres provisions réglementées *</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>610</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>612</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>614</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>616</small>
              </div>
              <div style={{ width: "151px" }}>&nbsp;</div>
            </div>
          </div>
        </div>
        <div className='p4table1r2 borderB'>
          <div
            style={{
              width: "1200px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "398px" }} className='borderR'>
                <small>Provisions pour risques et charges </small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>620</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>622</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>624</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>626</small>
              </div>
              <div style={{ width: "151px" }}>&nbsp;</div>
            </div>
          </div>
        </div>
        <div className='p4table1r2 borderB'>
          <div
            style={{
              width: "100px",
              display: "flex",
              alignItems: "center",
            }}
            className='borderR '
          >
            <small>Provisions pour dépréciation</small>
          </div>
          <div
            style={{
              width: "1100px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR borderB'>
                <small>Sur immobilisations</small>
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>630</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>632</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>634</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>636</small>
              </div>
              <div style={{ width: "151px" }} className=' borderB'>
                &nbsp;
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR borderB'>
                <small>Sur stocks et en cours</small>
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>640</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>642</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>644</small>
              </div>
              <div style={{ width: "151px" }} className='borderR borderB'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR borderB'>
                <small>646</small>
              </div>
              <div style={{ width: "151px" }} className=' borderB'>
                &nbsp;
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR'>
                <small>Sur comptes clients</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>650</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>652</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>654</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>656</small>
              </div>
              <div style={{ width: "151px" }}>&nbsp;</div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "301px" }} className='borderR'>
                <small>Autres provisions pour dépréciation</small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>660</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>662</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>664</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>666</small>
              </div>
              <div style={{ width: "151px" }}>&nbsp;</div>
            </div>
          </div>
        </div>
        <div className='p4table1r2'>
          <div
            style={{
              width: "1200px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                style={{
                  width: "398px",
                  fontWeight: "bold",
                  textAlign: "right",
                }}
                className='borderR'
              >
                <small>TOTAL </small>
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>680</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>682</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>684</small>
              </div>
              <div style={{ width: "151px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "49px" }} className='borderR'>
                <small>686</small>
              </div>
              <div style={{ width: "151px" }}>&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderR borderL'
      >
        <div
          style={{ width: "598px", display: "flex", flexDirection: "column" }}
          className='borderR'
        >
          <div style={{ textAlign: "justify" }} className='borderB'>
            <small>
              B MOUVEMENTS AFFECTANT LA PROVISION POUR AMORTISSEMENTS
              DEROGATOIRES
            </small>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "200px" }} className='borderR'>
              &nbsp;
            </div>
            <div style={{ width: "200px" }} className='borderR'>
              <small>Dotations</small>
            </div>
            <div style={{ width: "200px" }}>
              <small>Reprises</small>
            </div>
          </div>
        </div>
        <div style={{ width: "600px", textAlign: "justify" }}>
          <small>
            C VENTILATION DES DOTATIONS AUX PROVISIONS ET CHARGES À PAYER NON
            DÉDUCTIBLES POUR L'ASSIETTE DE L'IMPÔT (Si le cadre C est
            insuffisant, joindre un état du même modèle)
          </small>
        </div>
      </div>
      <div className='p4table2'>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div
            style={{ width: "199px", fontWeight: "bold" }}
            className='borderR'
          >
            <small>Fonds commercial</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>681</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>683</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Autres Immobilisations Incorporelles</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>700</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>705</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>1</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            <small>
              Indemnités pour congés à payer, charges sociales et fiscales
              correspondantes
            </small>
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Terrains</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>710</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>715</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>2</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Constructions </small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>720</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>725</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>3</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>
              TInstallations techniques, matériel et outillageerrains
            </small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>730</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>735</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>4</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Inst. générales, agencements et aménagements divers</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>740</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>745</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>5</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Matériel de transport</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>750</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>755</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>6</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "199px" }} className='borderR'>
            <small>Autres immobilisations corporelles</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>760</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>765</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>7</small>
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            &nbsp;
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div
            style={{ width: "199px", fontWeight: "bold", textAlign: "right" }}
            className='borderR'
          >
            <small>TOTAL</small>
          </div>
          <div style={{ width: "49px" }} className='borderR'>
            <small>770</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>775</small>
          </div>
          <div style={{ width: "150px" }} className='borderR'>
            &nbsp;
          </div>
          <div style={{ width: "400px" }} className='borderR'>
            <small>TOTAL à reporter ligne 322 du tableau n° 2033-B-SD</small>
          </div>
          <div style={{ width: "50px" }} className='borderR'>
            <small>780</small>
          </div>
        </div>
      </div>
      <div
        style={{ display: "flex", flexDirection: "row" }}
        className='borderR borderL borderB'
      >
        <div className='borderR'>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              fontWeight: "bold",
            }}
            className='borderB'
          >
            <div style={{ width: "49px" }} className='borderR'>
              <small>II</small>
            </div>
            <div style={{ width: "549px" }} className='borderR'>
              <small>DÉFICITS REPORTABLES</small>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
            className='borderB'
          >
            <div style={{ width: "400px" }} className='borderR'>
              <small>
                Déficits restant à reporter au titre de l'exercice précédent (1)
              </small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>982 bis</small>
            </div>
            <div style={{ width: "149px" }} className='borderR'>
              &nbsp;
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <div style={{ width: "150px" }} className='borderR'>
              <small>
                Déficits transférés de plein droit (article 209-II-2 du CGI)
              </small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>982</small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              &nbsp;
            </div>
            <div style={{ width: "149px" }} className='borderR'>
              <small>Nombre d’opérations sur l’exercice (2)</small>
            </div>
            <div style={{ width: "100px" }} className='borderR'>
              <small>982 ter</small>
            </div>
            <div style={{ width: "149px" }}>&nbsp;</div>
          </div>
        </div>
        <div className='borderB'></div>
      </div>
      <div
        style={{ display: "flex", flexDirection: "row" }}
        className='borderR borderL'
      >
        <div className='borderR'>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
            className='borderB'
          >
            <div style={{ width: "400px" }} className='borderR'>
              <small>Déficits imputés</small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>983</small>
            </div>
            <div style={{ width: "197px" }}>&nbsp;</div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
            className='borderB'
          >
            <div style={{ width: "400px" }} className='borderR'>
              <small>Déficits reportables</small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>984</small>
            </div>
            <div style={{ width: "197px" }}>&nbsp;</div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
            className='borderB'
          >
            <div style={{ width: "400px" }} className='borderR'>
              <small>Déficits de l’exercice</small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>860</small>
            </div>
            <div style={{ width: "197px" }}>&nbsp;</div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
            className='borderB'
          >
            <div style={{ width: "400px" }} className='borderR'>
              <small>Total des déficits restant à reporter</small>
            </div>
            <div style={{ width: "49px" }} className='borderR'>
              <small>870</small>
            </div>
            <div style={{ width: "197px" }}>&nbsp;</div>
          </div>
        </div>
        <div
          className='borderB'
          style={{ width: "600px", backgroundColor: "grey" }}
        ></div>
      </div>
      <div className='p4r2 borderB'>
        <div className='borderR'>
          <small>IV</small>
        </div>
        <div>
          <small>DIVERS</small>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='border'
      >
        <div
          style={{ width: "500px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>Primes et cotisations complémentaires facultatives</small>
        </div>
        <div style={{ width: "500px" }} className='borderR'>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div style={{ width: "300px" }} className='borderR'>
              <small>
                dont montant déductible des cotisations facultatives versées en
                application du I de l'article 154 bis du CGI dont cotisations
                facultatives Madelin (I de l’art. 154 bis du CGI)
              </small>
            </div>
            <div style={{ width: "50px" }} className='borderR'>
              <small>325</small>
            </div>
            <div style={{ width: "150px" }}>&nbsp;</div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "300px" }} className='borderR'>
              <small>
                dont cotisations facultatives aux nouveaux plans d'épargne
                retraite
              </small>
            </div>
            <div style={{ width: "50px" }} className='borderR'>
              <small>327</small>
            </div>
            <div style={{ width: "150px" }}>&nbsp;</div>
          </div>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>381</small>
          </div>
          <div style={{ width: "150px" }}>&nbsp;</div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "500px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>Cotisations personnelles obligatoires de l'exploitant *</small>
        </div>
        <div style={{ width: "500px" }} className='borderR'>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "300px" }} className='borderR'>
              <small>
                dont montant déductible des cotisations sociales obligatoires
                hors CSG-CRDS
              </small>
            </div>
            <div style={{ width: "50px" }} className='borderR'>
              <small>326</small>
            </div>
            <div style={{ width: "150px" }}>&nbsp;</div>
          </div>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>380</small>
          </div>
          <div style={{ width: "150px" }}>&nbsp;</div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>N° du centre de gestion agréé</small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>388</small>
          </div>
          <div
            style={{ width: "150px", display: "flex", flexDirection: "row" }}
          >
            <div style={{ width: "25px" }} className='borderR'></div>
            <div style={{ width: "25px" }} className='borderR'></div>
            <div style={{ width: "25px" }} className='borderR'></div>
            <div style={{ width: "25px" }} className='borderR'></div>
            <div style={{ width: "25px" }} className='borderR'></div>
            <div style={{ width: "25px" }}></div>
          </div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>Montant de la TVA collectée</small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>374</small>
          </div>
          <div style={{ width: "150px" }}></div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>
            Montant de la TVA déductible sur biens et services (sauf
            immobilisations)
          </small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>378</small>
          </div>
          <div style={{ width: "150px" }}></div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>Montant des prélèvements personnels de l'exploitant</small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>399</small>
          </div>
          <div style={{ width: "150px" }}></div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>
            Aides perçues ayant donné droit à la réduction d'impôt prévue au 4
            de l'article 238 bis du CGI pour l'entreprise donatrice{" "}
          </small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>398</small>
          </div>
          <div style={{ width: "150px" }}></div>
        </div>
      </div>
      <div
        style={{ width: "1200px", display: "flex", flexDirection: "row" }}
        className='borderB borderL borderR'
      >
        <div
          style={{ width: "1000px", display: "flex", alignItems: "center" }}
          className='borderR'
        >
          <small>
            Montant de l'investissement reçu qui a donné lieu à amortissement
            exceptionnel chez l'entreprise investisseur dans le cadre de
            l'article 217 octies du CGI
          </small>
        </div>
        <div style={{ width: "200px", display: "flex", flexDirection: "row" }}>
          <div style={{ width: "50px" }} className='borderR'>
            <small>397</small>
          </div>
          <div style={{ width: "150px" }}></div>
        </div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <small style={{ marginTop: "10px" }}>
          (1) Cette case correspond au montant porté sur la ligne 870 du tableau
          n° 2033-D-SD déposé au titre de l'exercice précédent.
        </small>
        <small style={{ marginTop: "10px" }}>
          * Des explications concernant ces rubriques figurent dans la notice n°
          2033-NOT-SD
        </small>
        <small style={{ marginTop: "10px" }}>
          (2) Indiquer, sur un feuillet séparé, l’identification, opération par
          opération, du nom de la société (et son n° siren) dont proviennent les
          déficits et le montant du transfert.
        </small>
      </div>
    </div>
  );
}
