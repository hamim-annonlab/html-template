import React from "react";
import { Box } from "../../components";
import "../../styles/page6.css";
import Header6 from "./Header6";

export default function Page6() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <Header6 />
      <div className='section1'>
        <div className='section1-row1'>
          <div className='exercice'>
            <small style={{ marginRight: "30px" }}>Exercice clos le</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='siren' style={{ marginLeft: "50px" }}>
            <small style={{ marginRight: "30px" }}>SIREN</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
        </div>
        <div className='section1-row2'>
          <div className='exercice'>
            <small style={{ marginRight: "70px" }}>
              Dénomination de l'entreprise
            </small>
            <div className='boxes'>
              <div
                style={{
                  height: "20px",
                  width: "300px",
                  borderBottom: "2px solid black",
                }}
              ></div>
            </div>
          </div>
        </div>
        <div className='section1-row3'>
          <div className='exercice'>
            <small style={{ marginRight: "50px" }}>Adresse (voie)</small>
            <div className='boxes'>
              <div
                style={{
                  height: "20px",
                  width: "400px",
                  borderBottom: "2px solid black",
                }}
              ></div>
            </div>
          </div>
        </div>
        <div className='section1-row4'>
          <div className='exercice'>
            <small style={{ marginRight: "30px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='siren' style={{ marginLeft: "50px" }}>
            <small style={{ marginRight: "30px" }}>Ville</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
        <table style={{ width: "100%", marginTop: "15px" }}>
          <tbody>
            <tr>
              <td style={{ width: "30%" }}>
                <small>
                  NOMBRE TOTAL D'ASSOCIES OU ACTIONNAIRES PERSONNES MORALES DE
                  L'ENTREPRISE
                </small>
              </td>
              <td style={{ width: "5%" }}>901</td>
              <td style={{ width: "15%" }}>&nbsp;</td>
              <td style={{ width: "30%" }}>
                <small>
                  NOMBRE TOTAL DE PARTS OU D'ACTIONS CORRESPONDANTES
                </small>
              </td>
              <td style={{ width: "5%" }}>902</td>
              <td style={{ width: "15%" }}>&nbsp;</td>
            </tr>
            <tr>
              <td style={{ width: "30%" }}>
                <small>
                  NOMBRE TOTAL D'ASSOCIES OU ACTIONNAIRES PERSONNES PHYSIQUES DE
                  L'ENTREPRISE
                </small>
              </td>
              <td style={{ width: "5%" }}>903</td>
              <td style={{ width: "15%" }}>&nbsp;</td>
              <td style={{ width: "30%" }}>
                <small>
                  NOMBRE TOTAL DE PARTS OU D'ACTIONS CORRESPONDANTES
                </small>
              </td>
              <td style={{ width: "5%" }}>904</td>
              <td style={{ width: "15%" }}>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className='p6section3'>
        <div
          style={{
            marginBottom: "10px",
            marginTop: "5px",
          }}
        >
          <small style={{ fontWeight: "700" }}>
            I – CAPITAL DÉTENU PAR LES PERSONNES MORALES :
          </small>
        </div>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Dénomination</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='p6section3' style={{ paddingTop: "10px" }}>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Dénomination</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='p6section3' style={{ paddingTop: "10px" }}>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Dénomination</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='p6section3' style={{ paddingTop: "10px" }}>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Forme-juridique</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Dénomination</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>
              N° SIREN (si société établie en France)
            </small>
            <div className='boxes'>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
              <Box height={20} width={25}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='p6section3'>
        <div
          style={{
            marginBottom: "10px",
            marginTop: "5px",
          }}
        >
          <small style={{ fontWeight: "700" }}>
            II – CAPITAL DÉTENU PAR LES PERSONNES PHYSIQUES :
          </small>
        </div>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Titre (2) </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='forme-juridique'>
            <small style={{ marginRight: "30px" }}>Nom patronymique</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "30px" }}>Prénom(s)</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>Nom marital</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Naissance :</small>
            <small style={{ marginRight: "10px" }}>Date</small>
            <div className='boxes'>
              <Box height={20} width={200}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>N° Département</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "20px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={160}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={160}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div className='p6section3' style={{ paddingTop: "10px" }}>
        <div className='p6row1'>
          <div className='forme-juridique'>
            <small style={{ marginRight: "60px" }}>Titre (2) </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='forme-juridique'>
            <small style={{ marginRight: "30px" }}>Nom patronymique</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "30px" }}>Prénom(s)</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
        <div className='p6row2'>
          <div className='siren'>
            <small style={{ marginRight: "60px" }}>Nom marital</small>
            <div className='boxes'>
              <Box height={20} width={300}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>% de détention</small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
          <div className='detention'>
            <small style={{ marginRight: "40px" }}>
              Nb de parts ou actions
            </small>
            <div className='boxes'>
              <Box height={20} width={100}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Naissance :</small>
            <small style={{ marginRight: "10px" }}>Date</small>
            <div className='boxes'>
              <Box height={20} width={200}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>N° Département</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "20px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={160}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={160}></Box>
            </div>
          </div>
        </div>
        <div className='p6row3'>
          <div className='Adresse'>
            <small style={{ marginRight: "60px" }}>Adresse</small>
            <small style={{ marginRight: "10px" }}>N°</small>
            <div className='boxes'>
              <Box height={20} width={80}></Box>
            </div>
          </div>
          <div className='voie'>
            <small style={{ marginRight: "40px" }}>Voie</small>
            <div className='boxes'>
              <Box height={20} width={600}></Box>
            </div>
          </div>
        </div>
        <div className='p6row4'>
          <div className='codepostal'>
            <small style={{ marginRight: "60px" }}>Code postal</small>
            <div className='boxes'>
              <Box height={20} width={150}></Box>
            </div>
          </div>
          <div className='commune'>
            <small style={{ marginRight: "40px" }}>Commune</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
          <div className='pays'>
            <small style={{ marginRight: "40px" }}>Pays</small>
            <div className='boxes'>
              <Box height={20} width={250}></Box>
            </div>
          </div>
        </div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <small style={{ marginTop: "5px" }}>
          (1) Lorsque le nombre d'associés excède le nombre de lignes du
          formulaire, veuillez utiliser un ou plusieurs tableaux
          supplémentaires. Dans ce cas, il convient de numéroter chaque tableau
          en haut et à gauche de la case prévue à cet effet et de porter le
          nombre total de tableaux souscrits en bas à droite de cette même case.
        </small>
        <small style={{ marginBottom: "20px", marginTop: "5px" }}>
          (2) Veuillez indiquer : « M » pour Monsieur, « MME » pour Madame.
        </small>
        <small>
          * Des explications concernant cette rubrique figurent dans la notice
          n° 2033-NOT-SD
        </small>
      </div>
    </div>
  );
}
