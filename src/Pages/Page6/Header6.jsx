import React from "react";
import { Box } from "../../components";
import "../../styles/Header7.css";

export default function Header6() {
  return (
    <div className='container' style={{ marginTop: "50px" }}>
      <div className='header7-row1'>
        <div>
          <p
            style={{
              height: "20px",
              width: "20px",
              border: "2px solid black",
              borderRadius: "50%",
              textAlign: "center",
            }}
          >
            6
          </p>
        </div>
        <div>
          <span
            style={{
              marginLeft: "40px",
              padding: "5px 40px",
              border: "2px solid black",
            }}
          >
            COMPOSITION DU CAPITAL SOCIAL
          </span>
        </div>
        <div style={{ marginLeft: "300px", textAlign: "center" }}>
          <p> DGFiP N° 2033-F-SD 2022</p>
          <small>
            Formulaire obligatoire (article 38 de l'annexe III au CGI)
          </small>
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "flex-end",
          marginBottom: "10px",
        }}
      >
        <div className='header7-section1' style={{ marginLeft: "30px" }}>
          <small>N° de dépôt</small>
          <Box height={25} width={100} />
        </div>
        <div
          style={{
            width: "400px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <div className='header7-section3'>
            <Box height={25} width={100} />
            <small style={{ marginLeft: "5px" }}>(1)</small>
          </div>
          <div className='header7-section4'>
            <small>Néant</small>
            <Box height={10} width={10} /> *
          </div>
        </div>
      </div>
    </div>
  );
}
