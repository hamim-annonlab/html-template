import styles from "./Page2.module.css";

export default function Page2() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <div className={styles.header}>
        <span>2</span>
        <p>COMPTE DE RÉSULTAT SIMPLIFIÉ DE L'EXERCICE (en liste)</p>
        <small>
          DGFiP <span className='bold'>N°</span> 2033-B-SD{" "}
          <span className='bold'>2022</span>
        </small>
      </div>
      <div className={styles.wrapper}>
        <div className={styles.rowOne}>
          <div>
            <small>
              Formulaire obligatoire (article 302 septies A bis du Code général
              des impôts)
            </small>
            <p>Désignation de l'entreprise</p>
          </div>
          <div>
            <div>
              <span></span>
              <span>
                Néant <i className='square-box-icon'></i>*
              </span>
            </div>
            <div>
              <span></span>
              <span
                className={`text-center text-sm ${styles.fixedCornerWidth}`}
              >
                Exercice N clos le
              </span>
            </div>
          </div>
        </div>
        <div className={styles.rowTwo}>
          <span className='bold'>A – RÉSULTAT COMPTABLE</span>
          <span>Formulaire déposé au titre de l'IR</span>
          <span>018</span>
          <span></span>
          <span>
            <span className={styles.boldSquares}>
              <i></i>
              <i></i>
            </span>
            <span className={styles.boldSquares}>
              <i></i>
              <i></i>
            </span>
            <span className={styles.boldSquares}>
              <i></i>
              <i></i>
              <i></i>
              <i></i>
            </span>
          </span>
        </div>
        <div className={styles.rowThree}>
          <div>
            <span>PRODUITS D'EXPLOITATION</span>
          </div>
          <div>
            <div>
              <div>
                <span>Ventes de marchandises *</span>
                <div>
                  <div>
                    <span>Production vendue</span>
                    <span>{"{"}</span>
                  </div>
                  <div>
                    <span>Biens</span>
                    <span>Services * </span>
                  </div>
                </div>
              </div>
              <div>dont export et livraisons intracommunautaires</div>
              <div>{"{"}</div>
              <div>
                <span>209</span>
                <span>215</span>
                <span>217</span>
              </div>
              <div>
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
            <div>
              Production stockée * ( Variation du stock en produits
              intermédiaires, produits finis et en cours de production )
            </div>
            <div>Production immobilisée *</div>
            <div>Subventions d'exploitations reçues</div>
            <div>Autres produits</div>
            <div className={`bold text-end ${styles.offsetRight5}`}>
              Total des produits d'exploitation hors TVA (I)
            </div>
          </div>
          <div>
            <span>210</span>
            <span>214</span>
            <span>218</span>
            <span>222</span>
            <span>224</span>
            <span>226</span>
            <span>230</span>
            <span>232</span>
          </div>
          <div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className={styles.rowFour}>
          <div>
            <span>CHARGES D'EXPLOITATION</span>
          </div>
          <div>
            <span>Achats de marchandises * (y compris droits de douane)</span>
            <span>Variation de stocks (marchandises) *</span>
            <span>
              Achats de matières premières et autres approvisionnements * (y
              compris droits de douane)
            </span>
            <span>
              Variation de stock (matières premières et approvisionnements) *
            </span>
            <span>
              <span>Autres charges externes * :</span>
              <span className={styles.offsetRight5}>
                (dont crédit-bail : - mobilier : ………………. - immobilier : ………………..
                )
              </span>
            </span>
            <span>
              <span>Impôts, taxes et versements assimilés</span>
              <span className={styles.offsetRight5}>
                (dont taxe professionnelle, CFE et CVAE *
              </span>
              <span>243</span>
              <span></span>
              <span>)</span>
            </span>
            <span>Rémunérations du personnel * </span>
            <span>Charges sociales (cf. renvoi 380)</span>
            <span>Dotations aux amortissements * </span>
            <span>Dotations aux provisions</span>
            <span>
              <span>
                Autres charges <span>{"{"}</span>
              </span>
              <span>
                <span>
                  dont provisions fiscales pour implantations commerciales à
                  l'étranger *
                </span>
                <span>
                  dont cotisations versées aux organisations syndicales et
                  professionnelles
                </span>
              </span>
              <span>
                <span>259</span>
                <span>260</span>
              </span>
              <span>
                <span></span>
                <span></span>
              </span>
              <span>{"}"}</span>
            </span>
            <span className='text-end bold'>
              Total des charges d'exploitation (II)
            </span>
          </div>
          <div>
            <span>234</span>
            <span>236</span>
            <span>238</span>
            <span>240</span>
            <span>242</span>
            <span>244</span>
            <span>250</span>
            <span>252</span>
            <span>254</span>
            <span>256</span>
            <span>262</span>
            <span className={`${styles.bgGrey} ${styles.borderNone}`}></span>
            <span>264</span>
          </div>
          <div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span className={styles.bgGrey}></span>
            <span></span>
          </div>
        </div>
        <div className={styles.rowFive}>
          <span className='bold'>1 – RÉSULTAT D'EXPLOITATION (I – II)</span>
          <span>270</span>
          <span></span>
        </div>
        <div className={styles.rowSix}>
          <span className='bold'>Produits financiers (III)</span>
          <span>270</span>
          <span></span>
          <span className={styles.bgGrey}></span>
          <span className='text-end'>Charges financières (V)</span>
          <span>294</span>
          <span></span>
        </div>
        <div className={styles.rowSeven}>
          <span>
            Charges exceptionnelles (VI) <span>{"{"}</span>
          </span>
          <span>
            <span>
              dont amortissements des souscriptions dans des PME innovantes
              (art. 217 octies)
            </span>
            <span>
              dont amortissements exceptionnels de 25 % des constructions
              nouvelles (art. 39 quinquies D)
            </span>
          </span>
          <span>
            <span>347</span>
            <span>348</span>
          </span>
          <span>
            <span></span>
            <span></span>
          </span>
          <span>{"}"}</span>
          <span>
            <span>300</span>
            <span className={`${styles.bgGrey} ${styles.borderNone}`}></span>
          </span>
          <span>
            <span></span>
            <span className={`${styles.bgGrey} ${styles.borderNone}`}></span>
          </span>
        </div>
        <div className={styles.rowFive}>
          <span className='bold'>Impôt sur les bénéfices * (VII)</span>
          <span>306</span>
          <span></span>
        </div>
        <div className={styles.rowFive}>
          <span className='bold'>
            2 – BÉNÉFICES OU PERTES : Produits (I + III + IV) – Charges (II + V
            + VI + VII)
          </span>
          <span>310</span>
          <span></span>
        </div>
        {/* section B  */}
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderT'
        >
          <div
            style={{ width: "250px", fontWeight: "bold" }}
            className='borderR'
          >
            <small>B – RÉSULTAT FISCAL</small>
          </div>
          <div
            style={{ width: "525px", textAlign: "center" }}
            className='borderR'
          >
            <small>
              Reporter le bénéfice comptable col. 1, le déficit comptable col. 2
            </small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>312</small>
          </div>
          <div style={{ width: "195px" }} className='borderR'>
            <small>&nbsp;</small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>314</small>
          </div>
          <div style={{ width: "172px" }}>&nbsp;</div>
        </div>
        {/* RÉINTÉGRATIONS */}
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderT'
        >
          <div style={{ width: "40px" }} className='borderR reintegrations'>
            <small className='reintegrationsabs'>RÉINTÉGRATIONS</small>
          </div>
          <div
            style={{ width: "732px", display: "flex", flexDirection: "column" }}
            className='borderR'
          >
            <div className='borderB'>
              <small>
                Rémunérations et avantages personnels non déductibles * 3
              </small>
            </div>
            <div className='borderB'>
              <small>
                Amortissements excédentaires (art. 39-4 du CGI) et autres
                amortissements non déductibles
              </small>
            </div>
            <div className='borderB'>
              <small>Provisions non déductibles *</small>
            </div>
            <div className='borderB'>
              <small>
                Impôts et taxes non déductibles * (cf. page 7 de la notice n°
                2033-NOT-SD)
              </small>
            </div>
            <div
              className='borderB'
              style={{ display: "flex", flexDirection: "row" }}
            >
              <div style={{ width: "300px" }} className='borderR'>
                <small>
                  Divers* dont intérêts excédentaires des cptes-cts d'associés
                </small>
              </div>
              <div style={{ width: "30px" }} className='borderR'>
                <small>247</small>
              </div>
              <div style={{ width: "60px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "250px" }} className='borderR'>
                <small>Ecarts de valeurs liquidatives sur OPC*</small>
              </div>
              <div style={{ width: "30px" }} className='borderR'>
                <small>248</small>
              </div>
              <div style={{ width: "60px" }}>&nbsp;</div>
            </div>
            <div
              className='borderB'
              style={{ display: "flex", flexDirection: "row" }}
            >
              <div style={{ width: "350px" }} className='borderR'>
                <small>
                  Fraction des loyers à réintégrer dans le cadre d'un créditbail
                  immobilier et de levée d'option
                </small>
              </div>
              <div style={{ width: "20px" }} className='borderR'>
                <small>(</small>
              </div>
              <div style={{ width: "270px" }} className='borderR'>
                <small>
                  Part de loyers dispensée de réintégration (art. 239 sexies D)
                </small>
              </div>
              <div style={{ width: "30px" }} className='borderR'>
                <small>248</small>
              </div>
              <div style={{ width: "40px" }} className='borderR'>
                &nbsp;
              </div>
              <div style={{ width: "20px" }}>
                <small>)</small>
              </div>
            </div>
            <div className='borderB'>
              <small>
                Charges afférentes à l'activité relevant du régime optionnel de
                taxation au tonnage des entreprises de transport maritime
              </small>
            </div>
            <div>
              <small>
                Résultat fiscal afférent à l'activité relevant du régime
                optionnel de taxation au tonnage des entreprises de transport
                maritime
              </small>
            </div>
          </div>
          <div style={{ width: "30px" }}>
            <div className='borderB borderR'>
              <small>316</small>
            </div>
            <div className='borderB borderR'>
              <small>318</small>
            </div>
            <div className='borderB borderR'>
              <small>322</small>
            </div>
            <div className='borderB borderR' style={{ padding: "9px 0" }}>
              <small>324</small>
            </div>
            <div className='borderB borderR' style={{ padding: "9px 0" }}>
              <small>330</small>
            </div>
            <div className='borderB borderR'>
              <small>251</small>
            </div>
            <div className='borderB borderR'>
              <small>998</small>
            </div>
            <div className=' borderR'>
              <small>999</small>
            </div>
          </div>
          <div style={{ width: "194px" }}>
            <div className='borderB borderR'>&nbsp;</div>
            <div className='borderB borderR'>&nbsp;</div>
            <div className='borderB borderR'>&nbsp;</div>
            <div className='borderB borderR'>&nbsp;</div>
            <div className='borderB borderR' style={{ padding: "9px 0" }}>
              &nbsp;
            </div>
            <div className='borderB borderR' style={{ padding: "9px 0" }}>
              &nbsp;
            </div>
            <div className='borderB borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
          </div>
          <div style={{ width: "30px", background: "#ccc" }}>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR' style={{ padding: "10px 0" }}>
              &nbsp;
            </div>
            <div className='borderR' style={{ padding: "10px 0" }}>
              &nbsp;
            </div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
          </div>
          <div style={{ width: "172px", background: "#ccc" }}>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div style={{ padding: "9px 0" }}>&nbsp;</div>
            <div style={{ padding: "9px 0" }}>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
          </div>
        </div>
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderT'
        >
          <div style={{ width: "40px" }} className='borderR borderB deductions'>
            <small className='deductionsabs'>DÉDUCTIONS</small>
          </div>
          <div
            style={{ width: "732px", display: "flex", flexDirection: "column" }}
          >
            <div>
              <div className='borderB'>
                <small>
                  Produits afférents à l'activité relevant du régime optionnel
                  de taxation au tonnage des entreprises de transport maritime
                </small>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "300px" }} className='borderR'>
                  <small>Entreprises nouvelles (44 sexies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>986</small>
                </div>
                <div style={{ width: "60px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small>ZFU – TE (44 octies et octies A)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>987</small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "300px" }} className='borderR'>
                  <small>
                    Reprise d'entreprises en difficulté (44 septies)
                  </small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>981</small>
                </div>
                <div style={{ width: "60px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small>JEI (44 sexies A)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>989</small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "300px" }} className='borderR'>
                  <small>ZRD (44 terdecies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>127</small>
                </div>
                <div style={{ width: "60px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small>ZRR (44 quindecies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>138</small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div style={{ width: "300px" }} className='borderR borderB'>
                  <small>Bassions d’emploi à redynamiser (44 duodecies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR borderB'>
                  <small>991</small>
                </div>
                <div style={{ width: "60px" }} className='borderR borderB'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small></small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small></small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "300px" }} className='borderR'>
                  <small>ZFANG (44 quaterdecies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>345</small>
                </div>
                <div style={{ width: "60px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small>Ecarts de valeurs liquidatives sur OPC*</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>344</small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "300px" }} className='borderR'>
                  <small>BUD (44 sexdecies)</small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>992</small>
                </div>
                <div style={{ width: "60px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "250px" }} className='borderR'>
                  <small>
                    Zone de développement prioritaire (44 septdecies)
                  </small>
                </div>
                <div style={{ width: "30px" }} className='borderR'>
                  <small>993</small>
                </div>
                <div style={{ width: "62px" }} className='borderR'>
                  &nbsp;
                </div>
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "32px" }} className='borderR borderB divers'>
                <small className='diversabs'>Dont divers</small>
              </div>
              <div
                style={{
                  width: "740px",
                }}
              >
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>
                      Créance due au titre du report en arrière du déficit
                    </small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>346</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>655</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies A)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>643</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies B)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>645</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies C)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>647</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies D)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>648</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>
                      Déduction exceptionnelle simulateur de conduite (art. 39
                      decies E)
                    </small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>641</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déductions exceptionnelles (art. 39 decies F)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>990</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
                <div
                  style={{ display: "flex", flexDirection: "row" }}
                  className='borderB'
                >
                  <div style={{ width: "610px" }} className='borderR'>
                    <small>Déduction exceptionnelle (art. 39 decies G)</small>
                  </div>
                  <div style={{ width: "30px" }} className='borderR'>
                    <small>649</small>
                  </div>
                  <div style={{ width: "62px" }} className='borderR'>
                    &nbsp;
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ width: "30px" }}>
            <div className='borderB'>&nbsp;</div>
            <div style={{ background: "#ccc" }}>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR borderB' style={{ padding: "11px 0" }}>
                &nbsp;
              </div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR borderB' style={{ paddingTop: "9px" }}>
                &nbsp;
              </div>
            </div>
          </div>
          <div style={{ width: "194px" }}>
            <div className='borderB borderR'>&nbsp;</div>
            <div style={{ background: "#ccc" }}>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderB borderR' style={{ padding: "11px 0" }}>
                &nbsp;
              </div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR borderB' style={{ paddingTop: "9px" }}>
                &nbsp;
              </div>
            </div>
          </div>
          <div style={{ width: "30px" }}>
            <div className='borderR borderB'>
              <small>997</small>
            </div>
            <div className='borderR borderB'>
              <small>342</small>
            </div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR'>&nbsp;</div>
            <div className='borderR borderB' style={{ padding: "11px 0" }}>
              &nbsp;
            </div>
            <div className='borderR borderB'>
              <small>350</small>
            </div>
            <div style={{ background: "#ccc" }}>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR'>&nbsp;</div>
              <div className='borderR borderB' style={{ paddingTop: "7px" }}>
                &nbsp;
              </div>
            </div>
          </div>
          <div style={{ width: "172px" }}>
            <div className='borderB'>&nbsp;</div>
            <div className='borderB'>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div className=' borderB' style={{ padding: "11px 0" }}>
              &nbsp;
            </div>
            <div className='borderB'>&nbsp;</div>
            <div style={{ background: "#ccc" }}>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div>&nbsp;</div>
              <div className='borderB' style={{ paddingTop: "7px" }}>
                &nbsp;
              </div>
            </div>
          </div>
        </div>
        {/* RÉSULTAT FISCAL AVANT */}
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "775px" }} className='borderR'>
            <small>
              RÉSULTAT FISCAL AVANT IMPUTATION DES DÉFICITS ANTÉRIEURS Bénéfice
              col.1 / Déficit col.2
            </small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>352</small>
          </div>
          <div style={{ width: "195px" }} className='borderR'>
            <small>&nbsp;</small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>354</small>
          </div>
          <div style={{ width: "172px" }}>&nbsp;</div>
        </div>

        {/* Déficits */}
        <div
          style={{ display: "flex", flexDirection: "row" }}
          className='borderB'
        >
          <div style={{ width: "40px" }} className='borderR deficit'>
            <small className='deficitabs'>Déficits</small>
          </div>
          <div>
            <div
              style={{ display: "flex", flexDirection: "row" }}
              className='borderB'
            >
              <div style={{ width: "736px" }} className='borderR'>
                <small>Déficit de l'exercice reporté en arrière *</small>
              </div>
              <div style={{ width: "30px" }} className='borderR'>
                <small>356</small>
              </div>
              <div style={{ width: "195px" }} className='borderR'>
                <small>&nbsp;</small>
              </div>
              <div
                style={{ width: "30px", background: "#ccc" }}
                className='borderR'
              >
                &nbsp;
              </div>
              <div style={{ width: "172px", background: "#ccc" }}>&nbsp;</div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                style={{ width: "736px", padding: "4px 0" }}
                className='borderR'
              >
                <small>
                  RÉSULTAT FISCAL AVANT IMPUTATION DES DÉFICITS ANTÉRIEURS
                  Bénéfice col.1 / Déficit col.2
                </small>
              </div>
              <div
                style={{ width: "30px", background: "#ccc" }}
                className='borderR'
              >
                &nbsp;
              </div>
              <div
                style={{ width: "195px", background: "#ccc" }}
                className='borderR'
              >
                <small>&nbsp;</small>
              </div>
              <div style={{ width: "30px" }} className='borderR'>
                <small>360</small>
              </div>
              <div style={{ width: "172px" }}>&nbsp;</div>
            </div>
          </div>
        </div>

        {/* RÉSULTAT FISCAL APRÈS */}
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div style={{ width: "775px" }} className='borderR'>
            <small>
              RÉSULTAT FISCAL APRÈS IMPUTATION DES DÉFICITS Bénéfice col.1 /
              Déficit col.2
            </small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>370</small>
          </div>
          <div style={{ width: "195px" }} className='borderR'>
            <small>&nbsp;</small>
          </div>
          <div style={{ width: "30px" }} className='borderR'>
            <small>372</small>
          </div>
          <div style={{ width: "172px" }}>&nbsp;</div>
        </div>
      </div>

      {/* footer */}
      <div style={{ display: "flex", flexDirection: "column" }}>
        <small style={{ marginTop: "10px" }}>
          * Des explications concernant ces rubriques figurent dans la notice n°
          2033-NOT-SD
        </small>
      </div>
    </div>
  );
}
