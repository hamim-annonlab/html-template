import React from "react";
import Box from "../../components/Box";
import "../../styles/Page1.css";

export default function Page1() {
  return (
    <div className='container' style={{ marginBottom: "80px" }}>
      <div style={{ width: "1200px" }}>
        <div
          className='p1header'
          style={{
            display: "flex",
            justifyContent: "flex-end",
            margin: "40px 0 20px",
          }}
        >
          <div>
            <p
              style={{
                height: "20px",
                width: "20px",
                border: "2px solid black",
                borderRadius: "50%",
                textAlign: "center",
              }}
            >
              1
            </p>
          </div>
          <span
            style={{
              marginLeft: "40px",
              padding: "5px 40px",
              height: "30px",
              border: "2px solid black",
            }}
          >
            BILAN SIMPLIFIÉ
          </span>
          <div
            style={{
              width: "200px",
              marginLeft: "350px",
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              gap: "5px",
            }}
          >
            <small>DGFiP N° 2033-A-SD 2022</small>
            <small
              style={{
                height: "30px",
                width: "80px",
                borderRadius: "50%",
                backgroundColor: "black",
                color: "white",
              }}
            >
              cerfa
            </small>
            <small style={{ fontWeight: "bold" }}>N° 15948 * 04</small>
            <small>
              Formulaire obligatoire (article 302 septies A du Code général des
              impôts)
            </small>
          </div>
        </div>
        <div className='border'>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div style={{ width: "200px" }} className='borderR'>
              <small>Désignation de l'entreprise</small>
            </div>
            <div style={{ width: "800px" }} className='borderR'>
              &nbsp;
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <small style={{ marginRight: "10px" }}>Neant</small>
              <Box height={10} width={10} /> <small>*</small>
            </div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "200px" }} className='borderR'>
              <small>Adresse de l'entreprise</small>
            </div>
            <div style={{ width: "1000px" }}>&nbsp;</div>
          </div>
        </div>
        <div className='border'>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              padding: "10px 40px",
            }}
          >
            <small>SIRET</small>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginLeft: "40px",
              }}
            >
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
              <Box height={25} width={25} />
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              padding: "10px 40px",
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <small>Durée de l'exercice en nombre de mois *</small>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  marginLeft: "40px",
                }}
              >
                <Box height={25} width={25} />
                <Box height={25} width={25} />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                marginLeft: "40px",
              }}
            >
              <small>Durée de l'exercice précédent *</small>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  marginLeft: "40px",
                }}
              >
                <Box height={25} width={25} />
                <Box height={25} width={25} />
              </div>
            </div>
          </div>
        </div>
        <div className='actif border'>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div style={{ width: "500px" }} className='borderR'>
              &nbsp;
            </div>
            <div style={{ width: "500px" }} className='borderR'>
              &nbsp;
            </div>
            <div style={{ width: "200px" }}>
              <small>Exercice N clos le</small>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  padding: "5px 0",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    margin: "0 5px",
                  }}
                >
                  <Box height={10} width={10} />
                  <Box height={10} width={10} />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    margin: "0 5px",
                  }}
                >
                  <Box height={10} width={10} />
                  <Box height={10} width={10} />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    margin: "0 5px",
                  }}
                >
                  <Box height={10} width={10} />
                  <Box height={10} width={10} />
                  <Box height={10} width={10} />
                  <Box height={10} width={10} />
                </div>
              </div>
            </div>
          </div>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div
              style={{
                width: "500px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
              className='borderR'
            >
              <p>ACTIF</p>
            </div>
            <div
              style={{ width: "500px", display: "flex", flexDirection: "row" }}
              className='borderR'
            >
              <div
                style={{
                  width: "250px",
                  display: "flex",
                  flexDirection: "column",
                  textAlign: "center",
                }}
                className='borderR'
              >
                <div className='borderB'>
                  <small>Brut</small>
                </div>
                <div>
                  <small>1</small>
                </div>
              </div>
              <div
                style={{
                  width: "250px",
                  display: "flex",
                  flexDirection: "column",
                  textAlign: "center",
                }}
              >
                <div className='borderB'>
                  <small>Amortissements – Provisions</small>
                </div>
                <div>
                  <small>2</small>
                </div>
              </div>
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "column",
                textAlign: "center",
              }}
            >
              <div className='borderB'>
                <small>Net</small>
              </div>
              <div>
                <small>3</small>
              </div>
            </div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "50px" }} className='borderR borderB'>
              <small className='actifImmobalize'>ACTIF IMMOBILISÉ</small>
            </div>
            <div style={{ width: "448px" }} className='borderR borderB'>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
                className='borderB'
              >
                <div
                  style={{
                    width: "248px",
                  }}
                >
                  <small>Immobilisations incorporelles </small>
                </div>
                <div
                  style={{
                    width: "200px",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div className='borderB'>
                    <small>Fonds commercial *</small>
                  </div>
                  <div>
                    <small>Autres *</small>
                  </div>
                </div>
              </div>
              <div className='borderB'>
                <small>Immobilisations corporelles *</small>
              </div>
              <div className='borderB'>
                <small>Immobilisations financières * (1)</small>
              </div>
              <div style={{ textAlign: "right", fontWeight: "bold" }}>
                <small>Total I (5)</small>
              </div>
            </div>
            <div
              style={{
                width: "500px",
                display: "flex",
                flexDirection: "row",
              }}
              className='borderR borderB'
            >
              <div style={{ width: "50px" }} className='borderR'>
                <div className='borderB'>
                  <small>010</small>
                </div>
                <div className='borderB'>
                  <small>014</small>
                </div>
                <div className='borderB'>
                  <small>028</small>
                </div>
                <div className='borderB'>
                  <small>040</small>
                </div>
                <div>
                  <small>044</small>
                </div>
              </div>
              <div style={{ width: "201px" }} className='borderR'>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div>&nbsp;</div>
              </div>
              <div style={{ width: "50px" }} className='borderR'>
                <div className='borderB'>
                  <small>012</small>
                </div>
                <div className='borderB'>
                  <small>016</small>
                </div>
                <div className='borderB'>
                  <small>030</small>
                </div>
                <div className='borderB'>
                  <small>042</small>
                </div>
                <div>
                  <small>048</small>
                </div>
              </div>
              <div style={{ width: "200px" }}>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div>&nbsp;</div>
              </div>
            </div>
            <div
              style={{
                width: "200px",
              }}
              className='borderB'
            >
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div>&nbsp;</div>
            </div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "50px" }} className='actifcirculant borderR'>
              <small className='actifcirculantabs'>ACTIF CIRCULANT</small>
            </div>
            <div style={{ width: "448px" }} className='borderR'>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                }}
                className='borderB'
              >
                <div
                  style={{
                    width: "48px",
                  }}
                  className='borderR stocks'
                >
                  <small className='stocksabs'>STOCKS</small>
                </div>
                <div
                  style={{
                    width: "400px",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div className='borderB' style={{ padding: "2px" }}>
                    <small>
                      Matières premières, approvisionnements, en cours de
                      production *
                    </small>
                  </div>
                  <div>
                    <small>Marchandises *</small>
                  </div>
                </div>
              </div>

              <div className='borderB'>
                <small>Avances et acomptes versés sur commandes</small>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
                className='borderB'
              >
                <div
                  style={{
                    width: "248px",
                  }}
                >
                  <small>Créances (2) </small>
                </div>
                <div
                  style={{
                    width: "200px",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div className='borderB'>
                    <small>Clients et comptes rattachés *</small>
                  </div>
                  <div>
                    <small>Autres * (3) </small>
                  </div>
                </div>
              </div>
              <div className='borderB'>
                <small>Valeurs mobilières de placement </small>
              </div>
              <div className='borderB'>
                <small>Disponibilités </small>
              </div>
              <div className='borderB'>
                <small>Charges constatées d'avance * </small>
              </div>
              <div
                style={{ textAlign: "right", fontWeight: "bold" }}
                className='borderB'
              >
                <small>Total II</small>
              </div>
              <div style={{ textAlign: "right", fontWeight: "bold" }}>
                <small>Total général (I + II)</small>
              </div>
            </div>
            <div
              style={{ width: "500px", display: "flex", flexDirection: "row" }}
              className='borderR'
            >
              <div style={{ width: "50px" }} className='borderR'>
                <div className='borderB' style={{ padding: "2px" }}>
                  <small>050</small>
                </div>
                <div className='borderB'>
                  <small>060</small>
                </div>
                <div className='borderB'>
                  <small>064</small>
                </div>
                <div className='borderB'>
                  <small>068</small>
                </div>
                <div className='borderB'>
                  <small>072</small>
                </div>
                <div className='borderB'>
                  <small>080</small>
                </div>
                <div className='borderB'>
                  <small>084</small>
                </div>
                <div className='borderB'>
                  <small>092</small>
                </div>
                <div className='borderB'>
                  <small>096</small>
                </div>
                <div>
                  <small>110</small>
                </div>
              </div>
              <div style={{ width: "201px" }} className='borderR'>
                <div className='borderB' style={{ padding: "2px" }}>
                  &nbsp;
                </div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div>&nbsp;</div>
              </div>
              <div style={{ width: "50px" }} className='borderR'>
                <div className='borderB' style={{ padding: "2px" }}>
                  <small>052</small>
                </div>
                <div className='borderB'>
                  <small>062</small>
                </div>
                <div className='borderB'>
                  <small>066</small>
                </div>
                <div className='borderB'>
                  <small>070</small>
                </div>
                <div className='borderB'>
                  <small>074</small>
                </div>
                <div className='borderB'>
                  <small>082</small>
                </div>
                <div className='borderB'>
                  <small>086</small>
                </div>
                <div className='borderB'>
                  <small>094</small>
                </div>
                <div className='borderB'>
                  <small>098</small>
                </div>
                <div>
                  <small>112</small>
                </div>
              </div>
              <div style={{ width: "200px" }}>
                <div className='borderB' style={{ padding: "2px" }}>
                  &nbsp;
                </div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div className='borderB'>&nbsp;</div>
                <div>&nbsp;</div>
              </div>
            </div>
            <div
              style={{
                width: "200px",
              }}
            >
              <div className='borderB' style={{ padding: "2px" }}>
                &nbsp;
              </div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div>&nbsp;</div>
            </div>
          </div>
        </div>
        <div className='passif border'>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div
              style={{
                width: "1000px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
              className='borderR'
            >
              <p>PASSIF</p>
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "column",
                textAlign: "center",
              }}
            >
              <div className='borderB'>
                <small> Exercice N NET</small>
              </div>
              <div>
                <small>1</small>
              </div>
            </div>
          </div>
          <div
            style={{ display: "flex", flexDirection: "row" }}
            className='borderB'
          >
            <div style={{ width: "50px" }} className='borderR capitaux'>
              <small className='capitauxabs'>CAPITAUX PROPRES</small>
            </div>
            <div
              style={{
                width: "900px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR'
            >
              <div className='borderB'>
                <small>Capital social ou individuel *</small>
              </div>
              <div className='borderB'>
                <small>Écarts de réévaluation</small>
              </div>
              <div className='borderB'>
                <small>Réserve légale</small>
              </div>
              <div className='borderB'>
                <small>Réserves réglementées * </small>
              </div>
              <div className='borderB' style={{ display: "flex" }}>
                <div style={{ width: "150px" }} className='borderR'>
                  <small>Autres réserves</small>
                </div>
                <div
                  style={{ width: "50px", textAlign: "center" }}
                  className='borderR'
                >
                  <small>(</small>
                </div>
                <div style={{ width: "500px" }} className='borderR'>
                  <small>
                    dont réserve relative à l'achat d'œuvres originales
                    d'artistes vivants) *
                  </small>
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>131</small>
                </div>
                <div style={{ width: "100px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "50px", textAlign: "center" }}>
                  <small>)</small>
                </div>
              </div>
              <div className='borderB'>
                <small>Report à nouveau</small>
              </div>
              <div className='borderB'>
                <small>Résultat de l'exercice</small>
              </div>
              <div className='borderB'>
                <small>Provisions réglementées</small>
              </div>
              <div style={{ textAlign: "right", fontWeight: "bold" }}>
                <small>Total I </small>
              </div>
            </div>
            <div
              style={{
                width: "48px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR'
            >
              <div className='borderB'>
                <small>120</small>
              </div>
              <div className='borderB'>
                <small>124</small>
              </div>
              <div className='borderB'>
                <small>126</small>
              </div>
              <div className='borderB'>
                <small>130</small>
              </div>
              <div className='borderB'>
                <small>132</small>
              </div>
              <div className='borderB'>
                <small>134</small>
              </div>
              <div className='borderB'>
                <small>136</small>
              </div>
              <div className='borderB'>
                <small>140</small>
              </div>
              <div>
                <small>142</small>
              </div>
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div>&nbsp;</div>
            </div>
          </div>
          <div
            className='borderB'
            style={{ display: "flex", flexDirection: "row" }}
          >
            <div style={{ width: "950px" }} className='borderR'>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <small> Provisions pour risques et charges </small>
                <small> Total II </small>
              </div>
            </div>
            <div style={{ width: "50px" }} className='borderR'>
              <small>154</small>
            </div>
            <div style={{ width: "200px" }}>&nbsp;</div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "50px" }} className='borderR borderB dettes'>
              <small className='dettesabs'>DETTES (4)</small>
            </div>
            <div
              style={{
                width: "900px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR borderB'
            >
              <div className='borderB'>
                <small>Emprunts et dettes assimilées</small>
              </div>
              <div className='borderB'>
                <small>Avances et acomptes reçus sur commandes en cours</small>
              </div>
              <div className='borderB'>
                <small>Fournisseurs et comptes rattachés *</small>
              </div>
              <div className='borderB' style={{ display: "flex" }}>
                <div style={{ width: "150px" }} className='borderR'>
                  <small>Autres dettes</small>
                </div>
                <div
                  style={{ width: "50px", textAlign: "center" }}
                  className='borderR'
                >
                  <small>(</small>
                </div>
                <div style={{ width: "500px" }} className='borderR'>
                  <small>
                    dont comptes courants d'associés de l'exercice N : ………..
                  </small>
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>169</small>
                </div>
                <div style={{ width: "100px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "50px", textAlign: "center" }}>
                  <small>)</small>
                </div>
              </div>
              <div className='borderB'>
                <small>Produits constatés d'avance</small>
              </div>
              <div style={{ textAlign: "right", fontWeight: "bold" }}>
                <small>Total III</small>
              </div>
            </div>
            <div
              style={{
                width: "48px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR borderB'
            >
              <div className='borderB'>
                <small>156</small>
              </div>
              <div className='borderB'>
                <small>164</small>
              </div>
              <div className='borderB'>
                <small>166</small>
              </div>
              <div className='borderB'>
                <small>172</small>
              </div>
              <div className='borderB'>
                <small>174</small>
              </div>
              <div>
                <small>176</small>
              </div>
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
              <div className='borderB'>&nbsp;</div>
            </div>
          </div>
          <div
            className='borderB'
            style={{ display: "flex", flexDirection: "row" }}
          >
            <div
              style={{ width: "953px", textAlign: "right", fontWeight: "bold" }}
              className='borderR'
            >
              <small>Total général ( I + II + III )</small>
            </div>
            <div style={{ width: "50px" }} className='borderR'>
              <small>180</small>
            </div>
            <div style={{ width: "200px" }}>&nbsp;</div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "50px" }} className='borderR renvois'>
              <small className='renvoisabs'>RENVOIS</small>
            </div>
            <div
              style={{
                width: "900px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR'
            >
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "50px" }} className='borderR'>
                  <small>(1)</small>
                </div>
                <div style={{ width: "300px" }} className='borderR'>
                  <small>
                    Dont immobilisations financières à moins d'un an
                  </small>
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>193</small>
                </div>
                <div style={{ width: "150px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>(4)</small>
                </div>
                <div style={{ width: "300px" }}>
                  <small>Dont dettes à plus d'un an</small>
                </div>
              </div>
              <div
                className='borderB'
                style={{ display: "flex", flexDirection: "row" }}
              >
                <div style={{ width: "50px" }} className='borderR'>
                  <small>(2)</small>
                </div>
                <div style={{ width: "300px" }} className='borderR'>
                  <small>Dont créances à plus d'un an</small>
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>197</small>
                </div>
                <div style={{ width: "150px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "300px" }}>
                  <small>
                    Coût de revient des immobilisations acquises ou créées au
                    cours de l'exercice *
                  </small>
                </div>
              </div>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>(3)</small>
                </div>
                <div style={{ width: "300px" }} className='borderR'>
                  <small>Dont compte courant d'associés débiteurs</small>
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>199</small>
                </div>
                <div style={{ width: "150px" }} className='borderR'>
                  &nbsp;
                </div>
                <div style={{ width: "50px" }} className='borderR'>
                  <small>(5)</small>
                </div>
                <div style={{ width: "300px" }}>
                  <small>
                    Prix de vente hors TVA des immobilisations cédées au cours
                    de l'exercice *
                  </small>
                </div>
              </div>
            </div>
            <div
              style={{
                width: "48px",
                display: "flex",
                flexDirection: "column",
              }}
              className='borderR'
            >
              <div className='borderB'>
                <small>195</small>
              </div>
              <div className='borderB' style={{ padding: "9px 0" }}>
                <small>182</small>
              </div>
              <div>
                <small>184</small>
              </div>
            </div>
            <div
              style={{
                width: "200px",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className='borderB'>&nbsp;</div>
              <div className='borderB' style={{ padding: "9px 0" }}>
                &nbsp;
              </div>
              <div>&nbsp;</div>
            </div>
          </div>
        </div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <small style={{ marginTop: "5px" }}>
            * Des explications concernant cette rubrique figurent dans la notice
            n° 2033-NOT-SD
          </small>
        </div>
      </div>
    </div>
  );
}
