import styles from "./Page3.module.css";

export default function Page3() {
  return (
    <section
      className='container'
      style={{ marginTop: "50px", marginBottom: "80px" }}
    >
      <div className={styles.header}>
        <span>3</span>
        <p>IMMOBILISATIONS – AMORTISSEMENTS – PLUS-VALUES – MOINS-VALUES</p>
        <small>
          DGFiP N° 2033-C-SD
          <span className='bold'>2022</span>
        </small>
      </div>

      <div className={styles.wrapper}>
        <div className={styles.rowOne}>
          <div>
            <small>
              Formulaire obligatoire (article 302 septies A bis du Code général
              des impôts)
            </small>
            <p>Désignation de l'entreprise</p>
          </div>
          <div>
            <div>
              <span></span>
              <span>
                Néant <i className='square-box-icon'></i>*
              </span>
            </div>
            <div>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
        <div className={styles.rowTwo}>
          <div>
            <div>
              <span>I</span>
              <span className='bold'>IMMOBILISATIONS</span>
            </div>
            <div>ACTIF IMMOBILISÉ</div>
          </div>
          <div>
            <div className='text-justify'>
              Valeur brute des immobilisations au début de l'exercice
            </div>
            <div className='text-center'>Augmentations</div>
            <div className='text-center'>Diminutions</div>
            <div className='text-justify'>
              Valeur brute des immobilisations à la fin de l'exercice
            </div>
            <div>
              <span className='text-center'>Réévaluation légale *</span>
              <span className='text-justify p-3px'>
                Valeur d'origine des immobilisations en fin d'exercice
              </span>
            </div>
          </div>
        </div>
        <div className={styles.rowThree}>
          <div>
            <div>Immobilisations incorporelles</div>
            <div>
              <span>Fonds commercial</span>
              <span>Autres</span>
            </div>
          </div>
          <div>
            <div>
              <div>
                <span>400</span>
                <span></span>
              </div>
              <div>
                <span>410</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>402</span>
                <span></span>
              </div>
              <div>
                <span>412</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>404</span>
                <span></span>
              </div>
              <div>
                <span>414</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>406</span>
                <span></span>
              </div>
              <div>
                <span>416</span>
                <span></span>
              </div>
            </div>
            <div>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
        <div className={styles.rowFour}>
          <div>
            <div>
              <span>Immobilisations corporelles</span>
            </div>
            <div>
              <span>Terrains</span>
              <span>Constructions</span>
              <span>
                Installations techniques, matériel et outillage industriels
              </span>
              <span>
                Installations générales, agencements, aménagements divers
              </span>
              <span>Matériel de transport</span>
              <span>Autres immobilisations corporelles</span>
            </div>
          </div>
          <div>
            <div>
              <div>
                <span>420</span>
                <span></span>
              </div>
              <div>
                <span>422</span>
                <span></span>
              </div>
              <div>
                <span>424</span>
                <span></span>
              </div>
              <div>
                <span>426</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div>
              <div>
                <span>430</span>
                <span></span>
              </div>
              <div>
                <span>432</span>
                <span></span>
              </div>
              <div>
                <span>434</span>
                <span></span>
              </div>
              <div>
                <span>436</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div style={{ height: 45 }}>
              <div>
                <span>440</span>
                <span></span>
              </div>
              <div>
                <span>442</span>
                <span></span>
              </div>
              <div>
                <span>444</span>
                <span></span>
              </div>
              <div>
                <span>446</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div style={{ height: 45 }}>
              <div>
                <span>450</span>
                <span></span>
              </div>
              <div>
                <span>452</span>
                <span></span>
              </div>
              <div>
                <span>454</span>
                <span></span>
              </div>
              <div>
                <span>456</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div>
              <div>
                <span>460</span>
                <span></span>
              </div>
              <div>
                <span>462</span>
                <span></span>
              </div>
              <div>
                <span>464</span>
                <span></span>
              </div>
              <div>
                <span>466</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div>
              <div>
                <span>470</span>
                <span></span>
              </div>
              <div>
                <span>472</span>
                <span></span>
              </div>
              <div>
                <span>474</span>
                <span></span>
              </div>
              <div>
                <span>476</span>
                <span></span>
              </div>
              <div></div>
            </div>
          </div>
        </div>
        <div className={styles.rowFive}>
          <div>
            <span>Immobilisations financières</span>
            <span>TOTAL</span>
          </div>
          <div>
            <div>
              <div>
                <span>480</span>
                <span></span>
              </div>
              <div>
                <span>482</span>
                <span></span>
              </div>
              <div>
                <span>484</span>
                <span></span>
              </div>
              <div>
                <span>486</span>
                <span></span>
              </div>
              <div></div>
            </div>
            <div>
              <div>
                <span>490</span>
                <span></span>
              </div>
              <div>
                <span>492</span>
                <span></span>
              </div>
              <div>
                <span>494</span>
                <span></span>
              </div>
              <div>
                <span>496</span>
                <span></span>
              </div>
              <div></div>
            </div>
          </div>
        </div>
        <div className={styles.rowSix}>
          <div>
            <div>
              <span>II</span>
              <span className='bold'>AMORTISSEMENTS</span>
            </div>
            <div>IMMOBILISATIONS AMORTISSABLES</div>
          </div>
          <div>
            <span>Montant des amortissements au début de l'exercice</span>
            <span>Augmentations : dotations de l'exercice</span>
            <span>
              Diminutions : amortissements afférents aux éléments sortis de
              l'actif et reprises
            </span>
            <span>Montant des amortissements à la fin de l'exercice</span>
          </div>
        </div>
        <div className={styles.rowSeven}>
          <div>
            <span>Fonds commercial</span>
            <span>Autres immobilisations incorporelles</span>
          </div>
          <div>
            <div>
              <div>
                <span>495</span>
                <span></span>
              </div>
              <div>
                <span>497</span>
                <span></span>
              </div>
              <div>
                <span>498</span>
                <span></span>
              </div>
              <div>
                <span>499</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>500</span>
                <span></span>
              </div>
              <div>
                <span>502</span>
                <span></span>
              </div>
              <div>
                <span>504</span>
                <span></span>
              </div>
              <div>
                <span>504</span>
                <span></span>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.rowEight}>
          <div>
            <div>
              <span>Immobilisations corporelles</span>
            </div>
            <div>
              <span>Terrains</span>
              <span>Constructions</span>
              <span>
                Installations techniques, matériel et outillage industriels
              </span>
              <span>
                Installations générales, agencements, aménagements divers
              </span>
              <span>Matériel de transport</span>
              <span>Autres immobilisations corporelles</span>
            </div>
          </div>
          <div>
            <div>
              <div>
                <span>510</span>
                <span></span>
              </div>
              <div>
                <span>512</span>
                <span></span>
              </div>
              <div>
                <span>514</span>
                <span></span>
              </div>
              <div>
                <span>516</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>520</span>
                <span></span>
              </div>
              <div>
                <span>522</span>
                <span></span>
              </div>
              <div>
                <span>524</span>
                <span></span>
              </div>
              <div>
                <span>526</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>530</span>
                <span></span>
              </div>
              <div>
                <span>532</span>
                <span></span>
              </div>
              <div>
                <span>534</span>
                <span></span>
              </div>
              <div>
                <span>536</span>
                <span></span>
              </div>
            </div>
            <div style={{ minHeight: 45 }}>
              <div>
                <span>540</span>
                <span></span>
              </div>
              <div>
                <span>542</span>
                <span></span>
              </div>
              <div>
                <span>544</span>
                <span></span>
              </div>
              <div>
                <span>546</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>550</span>
                <span></span>
              </div>
              <div>
                <span>552</span>
                <span></span>
              </div>
              <div>
                <span>554</span>
                <span></span>
              </div>
              <div>
                <span>556</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>560</span>
                <span></span>
              </div>
              <div>
                <span>562</span>
                <span></span>
              </div>
              <div>
                <span>564</span>
                <span></span>
              </div>
              <div>
                <span>566</span>
                <span></span>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.rowNine}>
          <div>TOTAL</div>
          <div>
            <div>
              <span>570</span>
              <span></span>
            </div>
            <div>
              <span>572</span>
              <span></span>
            </div>
            <div>
              <span>574</span>
              <span></span>
            </div>
            <div>
              <span>576</span>
              <span></span>
            </div>
          </div>
        </div>
        <div className={styles.rowTen}>
          <div>III</div>
          <div>
            <span className='bold'>
              PLUS-VALUES, MOINS-VALUES à 19 %, 15 % et 0 % pour les entreprises
              à l'IS et 12,8 % pour les entreprises à l'IR
            </span>{" "}
            (Si ce cadre est insuffisant, joindre un état du même modèle)
          </div>
        </div>
        <div className={styles.rowEleven}>
          <div>
            Nature des immobilisations cédées virées de poste à poste, mise hors
            service ou réintégrées dans le patrimoine privé
          </div>
          <div>
            <div>
              <div>
                <span>1</span>
                <span></span>
              </div>
              <div>
                <span>2</span>
                <span></span>
              </div>
              <div>
                <span>3</span>
                <span></span>
              </div>
              <div>
                <span>4</span>
                <span></span>
              </div>
              <div>
                <span>5</span>
                <span></span>
              </div>
            </div>
            <div>
              <div>
                <span>6</span>
                <span></span>
              </div>
              <div>
                <span>7</span>
                <span></span>
              </div>
              <div>
                <span>8</span>
                <span></span>
              </div>
              <div>
                <span>9</span>
                <span></span>
              </div>
              <div>
                <span>10</span>
                <span></span>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.rowTwelve}>
          <div>
            <span>Immobilisations</span>
          </div>
          <div>
            <div>
              <span>Valeur d'actif *</span>
              <span>Amortissements*</span>
              <span>Valeur résiduelle</span>
              <span>Prix de cession*</span>
            </div>
            <div>
              <span>
                <i>1</i>
              </span>
              <span>
                <i>2</i>
              </span>
              <span>
                <i>3</i>
              </span>
              <span>
                <i>4</i>
              </span>
            </div>
          </div>
          <div>
            <div>
              <div>Plus ou moins-values</div>
              <div>
                <span>Court terme*</span>
                <span>Long terme</span>
              </div>
            </div>
            <div>
              <span>
                <i>5</i>
              </span>
              <span>
                19 %<i>6</i>
              </span>
              <span>
                15 % ou 12,8 %<i>7</i>
              </span>
              <span>
                0 %<i>8</i>
              </span>
            </div>
          </div>
        </div>
        <div className={styles.rowThirteen}>
          <div>
            <div>1</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>2</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>3</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>4</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>5</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>6</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>7</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>8</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>9</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div>
            <div>10</div>
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
        <div className={styles.rowFourteen}>
          <div>Total</div>
          <div>
            <div>
              <span>578</span>
              <span></span>
            </div>
            <div>
              <span>580</span>
              <span></span>
            </div>
            <div>
              <span>582</span>
              <span></span>
            </div>
            <div>
              <span>584</span>
              <span></span>
            </div>
            <div>
              <span>586</span>
              <span></span>
            </div>
            <div>
              <span>581</span>
              <span></span>
            </div>
            <div>
              <span>587</span>
              <span></span>
            </div>
            <div>
              <span>589</span>
              <span></span>
            </div>
          </div>
        </div>
        <div className={styles.rowFifteen}>
          <div>
            <div>Plus-values taxables à 19 % (1)</div>
            <div>
              <span>579</span>
              <span></span>
            </div>
            <div>Régularisations</div>
          </div>
          <div>
            <div>
              <span>590</span>
              <span></span>
            </div>
            <div>
              <span>583</span>
              <span></span>
            </div>
            <div>
              <span>594</span>
              <span></span>
            </div>
            <div>
              <span>595</span>
              <span></span>
            </div>
          </div>
        </div>
        <div className={styles.rowSixteen}>
          <div className='bold'>TOTAL</div>
          <div>
            <div>
              <span>596</span>
              <span></span>
            </div>
            <div>
              <span>585</span>
              <span></span>
            </div>
            <div>
              <span>597</span>
              <span></span>
            </div>
            <div>
              <span>599</span>
              <span></span>
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          marginTop: "10px",
          gap: "5px",
        }}
      >
        <small>
          * Des explications concernant ces rubriques figurent dans la notice n°
          2033-NOT-SD
        </small>
        <small>
          (1) Ces plus-values sont imposables au taux de 19 % en application des
          articles 238 bis JA, 210 F et 208 C du CGI. Joindre un état établi
          selon le même modèle, indiquant les modalités de calcul de ces
          plus-values.
        </small>
      </div>
    </section>
  );
}
